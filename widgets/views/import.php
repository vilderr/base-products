<?php

use yii\helpers\Html;
use yii\helpers\Json;

?>
<div class="panel panel-default flat">
    <div class="panel-body" id="import-result-container">
        <?= Html::tag('p', 'Для начала импорта нажмите кнопку "Запустить".<br>', ['class' => 'lead']); ?>
        <?= Html::tag('p', 'Не запускайте импорт в нескольких вкладках браузера!<br>Вы можете остановить операцию во время
            импорта, нажав кнопку "Остановить".');?>
    </div>
    <div class="panel-footer clearfix">
        <?= Html::submitButton('Запустить', [
            'id' => 'start-button',
            'class' => 'btn btn-info btn-sm float-left mr-1',
            'onclick' => 'StartImport(' . Json::encode(['import' => 'Y']) . ');',
        ]); ?>
        <?= Html::submitButton('Остановить', [
            'class' => 'btn btn-outline-info btn-sm float-left',
            'onclick' => 'EndImport();',
        ]); ?>
    </div>
</div>
