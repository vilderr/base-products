<?php

namespace vilderr\baseproducts\widgets;

use vilderr\main\helpers\StringHelper;
use vilderr\reference\models\Property;
use yii\base\Widget;
use yii\caching\TagDependency;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\Alert;
use yii\helpers\Json;
use yii\httpclient\Client;
use vilderr\main\Yii;
use vilderr\main\helpers\ModelHelper;
use vilderr\reference\models\element\Element;
use vilderr\reference\models\reference\Reference;
use vilderr\baseproducts\models\Section;
use vilderr\reference\models\Type;
use vilderr\sale\models\Product;
use vilderr\sale\models\Price;
use vilderr\sale\models\price\PriceType;
use vilderr\baseproducts\Module;
use vilderr\baseproducts\models\ImportTree;
use vilderr\baseproducts\widgets\assets\ImportAsset;

/**
 * Class Import
 * @package vilderr\baseproducts\widgets
 */
class Import extends Widget
{
    public $params = [];

    public $ns;

    /** @var Reference */
    protected $reference;
    protected $errors = [];
    /** @var PriceType[] */
    protected $priceTypes = [];

    /**
     * @return string
     * @throws \Throwable
     * @throws \yii\base\ExitException
     */
    public function run()
    {
        $request = Yii::$app->request;
        ImportAsset::register($this->view);

        if ($request->isPost && ($request->post('import') && $request->post('import') === 'Y')) {
            $post = Yii::$app->request->post();
            if (isset($post['NS']) && is_array($post['NS'])) {
                $this->ns = $post['NS'];
            } else {
                $this->ns = [
                    'step' => 0,
                    'last_id' => 0,
                    'done' => [
                        'all' => 0,
                        'crc' => 0,
                        'add' => 0,
                        'upd' => 0,
                        'err' => 0,
                        'del' => 0
                    ],
                ];
            }

            if ($this->ns['step'] < 1) {
                try {
                    Yii::$app->db->createCommand()->truncateTable(ImportTree::tableName())->execute();
                    $this->ns['step']++;
                } catch (Exception $exception) {
                    $this->errors[] = $exception->getMessage();
                }
            } elseif ($this->ns['step'] < 2) {
                try {
                    $catalogReferenceType = $this->checkReferenceType('baseproducts', 'Каталог товаров');
                    $catalogReference = $this->checkReference($catalogReferenceType->id, 'Каталог товаров', 'baseproducts');
                    $this->ns['catalog_reference_id'] = $catalogReference->id;

                    $propReferenceType = $this->checkReferenceType('baseproducts_properties', 'Свойства каталога');
                    $this->ns['prop_reference_type_id'] = $propReferenceType->id;

                    if ($catalogReference->is_catalog === 'Y') {
                        $this->checkPrices();
                    }

                    $this->ns['step']++;
                } catch (\Exception $exception) {
                    $this->errors[] = $exception->getMessage();
                }
            } elseif ($this->ns['step'] < 3) {
                $this->reference = Reference::findOne($this->ns['catalog_reference_id']);
                $sections = ArrayHelper::getValue($this->params, 'import_sections');
                $allowSections = ArrayHelper::getValue($this->params, 'allow_sections', false);

                if (is_array($sections) && !empty($sections) && $allowSections) {
                    try {
                        $result = $this->importSections($sections, time(), 30);
                        $counter = 0;
                        foreach ($result as $key => $value) {
                            $this->ns['done'][$key] += $value;
                            $counter += $value;
                        }

                        if (!$counter) {
                            $this->ns['last_id'] = 0;
                            $this->ns['done'] = [
                                'add' => 0,
                                'upd' => 0,
                                'err' => 0,
                                'crc' => 0,
                                'del' => 0,
                                'dea' => 0
                            ];
                            $this->ns['step']++;
                        }
                    } catch (\Exception $exception) {
                        $this->errors[] = $exception->getMessage();
                    }
                } else {
                    $this->ns['step']++;
                }
            } elseif ($this->ns['step'] < 4) {
                $sections = ArrayHelper::getValue($this->params, 'import_sections', []);
                $this->reference = Reference::findOne($this->ns['catalog_reference_id']);
                $this->priceTypes = $this->checkPrices();

                try {
                    $result = $this->importElements($sections, time(), 30);
                    $counter = 0;
                    foreach ($result as $key => $value) {
                        $this->ns['done'][$key] += $value;
                        $counter += $value;
                    }

                    if (!$counter) {
                        $this->ns['last_id'] = 0;
                        $this->ns['step']++;
                    }
                } catch (\Exception $exception) {
                    $this->errors[] = $exception->getMessage();
                }
            }

            if (!empty($this->errors)) {
                foreach ($this->errors as $error) {
                    echo Html::tag('p', $error, ['class' => 'alert alert-danger']);
                }
            }

            if (!count($this->errors)) {
                $body = '';
                if ($this->ns["step"] < 4) {
                    if ($this->ns['step'] < 2) {
                        $body .= Html::tag('h5', 'Импорт метаданных каталога');
                    } elseif ($this->ns['step'] < 3) {
                        $body .= Html::tag('h5', 'Импорт категорий каталога');
                        foreach ($this->ns['done'] as $k => $v) {
                            $body .= Html::tag('div', Html::tag('span', Module::t($k, ['value' => $v])));
                        }
                    } elseif ($this->ns['step'] < 4) {
                        $body .= Html::tag('h5', 'Импорт товаров');
                        foreach ($this->ns['done'] as $k => $v) {
                            $body .= Html::tag('div', Html::tag('span', Module::t($k, ['value' => $v])));
                        }
                    }

                    echo Alert::widget([
                        'closeButton' => false,
                        'body' => $body,
                        'options' => [
                            'class' => 'alert alert-info'
                        ]
                    ]);

                    echo '<script>DoNext(' . Json::encode(['NS' => $this->ns, "import" => "Y"]) . ');</script>';
                } else {
                    $body .= Html::tag('h5', 'Импорт завершен');
                    foreach ($this->ns['done'] as $k => $v) {
                        $body .= Html::tag('div', Html::tag('span', Module::t($k, ['value' => $v])));
                    }

                    echo Alert::widget([
                        'closeButton' => false,
                        'body' => $body,
                        'options' => [
                            'class' => 'alert alert-success'
                        ]
                    ]);

                    echo '<script>EndImport();</script>';
                }
            } else {
                echo '<script>EndImport();</script>';
            }

            Yii::$app->end();
        }

        return $this->render('import');
    }

    /**
     * @return Type
     * @throws \Throwable
     */
    protected function checkReferenceType($id, $name)
    {
        if (!$model = Type::findOne(['id' => $id])) {
            $model = new Type(['id' => $id, 'name' => $name]);

            $transaction = $model::getDb()->beginTransaction();
            try {
                $res = $model->insert();
                if ($res === false) {
                    throw new \Exception('Ошибка сохранения типа справочников');
                }
                $transaction->commit();
            } catch (\Exception $exception) {
                $transaction->rollBack();
                throw $exception;
            } catch (\Throwable $exception) {
                $transaction->rollBack();
                throw $exception;
            }
        }

        return $model;
    }

    /**
     * @param Type $type
     *
     * @return Reference
     * @throws \Throwable
     */
    protected function checkReference($type_id, $name, $slug = null)
    {
        $model = Reference::findOne(['type_id' => $type_id, 'name' => $name, 'slug' => $slug]);
        if (!$model) {
            $model = new Reference(['type_id' => $type_id, 'name' => $name, 'slug' => $slug, 'status' => ModelHelper::STATUS_ACTIVE]);
            if ($slug == 'baseproducts' && Yii::$app->hasModule('sale')) {
                $model->is_catalog = ModelHelper::STATUS_ACTIVE;
            }
        }

        $transaction = $model::getDb()->beginTransaction();
        try {
            $res = $model->save();
            if ($res === false) {
                throw new \Exception('Ошибка сохранения справочника');
            }
            $transaction->commit();
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return $model;
    }

    /**
     * @return array|PriceType[]
     * @throws \Throwable
     */
    protected function checkPrices()
    {
        $types = PriceType::find()->orderBy(['base' => SORT_DESC, 'sort' => SORT_ASC, 'name' => SORT_ASC, 'id' => SORT_ASC])->indexBy('external_id')->all();
        if (!array_key_exists('price', $types)) {
            $type = new PriceType(['name' => 'Цена', 'base' => 'Y', 'external_id' => 'price']);
            $res = $type->insert();
            if ($res === false) {
                throw new \Exception('Ошибка при создании типа цены');
            }

            $types['price'] = $type;
        }

        if (!array_key_exists('oldprice', $types)) {
            $type = new PriceType(['name' => 'Старая цена', 'external_id' => 'oldprice']);
            $res = $type->insert();
            if ($res === false) {
                throw new \Exception('Ошибка при создании типа цены');
            }

            $types['oldprice'] = $type;
        }

        return $types;
    }

    /**
     * @param $sections
     * @param $startTime
     * @param $interval
     *
     * @return array
     * @throws \Throwable
     */
    protected function importSections($sections, $startTime, $interval)
    {
        $counter = [
            'crc' => 0,
            'add' => 0,
            'upd' => 0,
            'err' => 0,
        ];

        try {
            $url = ArrayHelper::getValue($this->params, 'import_sections_url');
            $token = ArrayHelper::getValue($this->params, 'import_api_key');
            $result = $this->getRemoteSections($url, $token, 2, $sections, $this->ns['last_id']);

            foreach ($result as $item) {
                $counter['crc']++;

                $category = $this->importSection($item, $counter);
                $this->ns['last_id']++;
                if ($interval > 0 && (time() - $startTime) > $interval)
                    break;
            }
        } catch (\Exception $exception) {
            $this->errors[] = $exception->getMessage();
        }

        return $counter;
    }

    /**
     * @param $item
     * @param $counter
     *
     * @return Section
     * @throws \Throwable
     */
    protected function importSection($item, &$counter)
    {
        $external_id = $item['external_id'];
        $tmp_id = $this->getArrayCrc($item);

        if (!$section = Section::findOne(['external_id' => $external_id])) {
            $section = new Section(['reference_id' => $this->reference->id]);
        }

        if ($section && $section->tmp_id == $tmp_id) {
            $counter['upd']++;

            return $section;
        }

        if (!$parent = $section->section) {
            $parent = Section::findOne(['external_id' => $item['category']['external_id']]);
        }

        $attributes = [
            'name' => $item['name'],
            'section_id' => ($parent && $item['category_id'] > 1) ? $parent->id : null,
            'slug' => StringHelper::translit($item['name'], 'ru', ['replace_space' => '_', 'replace_other' => '_']),
            'external_id' => $item['external_id'],
            'tmp_id' => $tmp_id,
            'status' => ModelHelper::STATUS_ACTIVE,
            'base_section_id' => intval($item['id'])
        ];

        $section->setAttributes($attributes);

        $transaction = $section::getDb()->beginTransaction();
        try {
            $res = $section->save();
            if ($res === false) {
                $counter['err']++;
                throw new \Exception('Ошибка сохранения категории');
            }
            $transaction->commit();
            $counter['add']++;

        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw $exception;
        }

        return $section;
    }

    /**
     * @param $item
     * @param $counter
     *
     * @return array
     * @throws \Throwable
     */
    protected function checkProperty($name)
    {
        $external_id = md5($name);
        if (!$model = Property::findOne(['external_id' => $external_id, 'reference_id' => $this->ns['catalog_reference_id']])) {
            $reference = $this->checkReference($this->ns['prop_reference_type_id'], $name);
            $attributes = [
                'reference_id' => $this->ns['catalog_reference_id'],
                'name' => $name,
                'status' => ModelHelper::STATUS_ACTIVE,
                'slug' => StringHelper::translit($name, 'ru', ['replace_space' => '_', 'replace_other' => '_']),
                'external_id' => $external_id,
                'property_type' => Property::TYPE_LINK_ELEMENT,
                'link_reference_id' => $reference->id,
                'multiply' => ModelHelper::STATUS_ACTIVE
            ];

            $model = new Property();
            $model->loadDefaultValues();
            $model->setAttributes($attributes);

            $transaction = $model::getDb()->beginTransaction();
            try {
                $res = $model->insert();
                if ($res === false) {
                    throw new \Exception('Ошибка сохранения свойства каталога');
                }

                $transaction->commit();

            } catch (\Exception $exception) {
                $transaction->rollBack();
                throw $exception;
            } catch (\Throwable $exception) {
                $transaction->rollBack();
                throw $exception;
            }
        }

        return ArrayHelper::toArray($model);
    }

    protected function checkPropertyValue($arProperty, $value)
    {
        $result = null;
        switch ($arProperty['property_type']) {
            case Property::TYPE_LINK_ELEMENT:
                $externalId = md5($value);
                if (!$model = Element::find()->forReference($arProperty['link_reference_id'])->andWhere(['external_id' => $externalId])->one()) {
                    $value = Html::decode($value);

                    $attributes = [
                        'reference_id' => $arProperty['link_reference_id'],
                        'name' => $value,
                        'external_id' => $externalId,
                        'slug' => StringHelper::translit($value, 'ru', ['replace_space' => '_', 'replace_other' => '_']),
                        'status' => ModelHelper::STATUS_ACTIVE
                    ];

                    $model = new Element();
                    $model->loadDefaultValues();
                    $model->setAttributes($attributes);
                    $transaction = $model::getDb()->beginTransaction();
                    try {
                        $res = $model->insert();
                        if ($res === false) {
                            throw new \Exception('Ошибка сохранения значения свойства');
                        }

                        $transaction->commit();
                    } catch (\Exception $exception) {
                        $transaction->rollBack();
                        throw $exception;
                    } catch (\Throwable $exception) {
                        $transaction->rollBack();
                        throw $exception;
                    }
                }

                $result = $model->id;

                break;
        }

        return $result;
    }

    protected function importElements(array $sections, $startTime, $interval)
    {
        $counter = [
            'crc' => 0,
            'add' => 0,
            'upd' => 0,
            'err' => 0,
        ];

        $url = ArrayHelper::getValue($this->params, 'import_products_url');
        $token = ArrayHelper::getValue($this->params, 'import_api_key');
        $base_url = ArrayHelper::getValue($this->params, 'base_url');

        $sections = implode(',', $sections);
        $result = $this->sendRequest($url, $token, [
            'id' => $this->ns['last_id'],
            'limit' => 50,
            'sections' => $sections
        ]);

        $result = Json::decode($result);
        foreach ($result as $item) {
            $counter['crc']++;
            if (isset($item['picture']))
                $item['picture'] = $base_url . $item['picture'];

            $product = $this->importElement($item, $counter);

            $this->ns['last_id'] = $item['id'];

            if ($interval > 0 && (time() - $startTime) > $interval)
                break;
        }

        return $counter;
    }

    protected function importElement($item, &$counter)
    {
        $external_id = $item['external_id'];
        $tmp_id = $this->getArrayCrc($item);

        $attributes = [
            'name' => htmlspecialchars_decode($item['name']),
            'tmp_id' => $tmp_id
        ];

        $newRecord = false;

        if ($element = Element::findOne(['external_id' => $external_id])) { // не грузим элементы, которые не изменялись через админку
            if ($element->tmp_id == $tmp_id) {
                $counter['upd']++;

                // временно обновляем updated_at для товаров, пропущенных в импорте
                $DB = $element::getDb();
                $DB->createCommand()->update($element::tableName(), ['updated_at' => time()], ['id' => $element->id])->execute();

                return $element;
            }
        } else {
            // TODO не грузим элементы у которых не существует категория
            if (!$section = Section::find()->where(['reference_id' => $this->reference->id, 'base_section_id' => $item['category_id']])->cache(86400, new TagDependency(['tags' => 'Reference:' . $this->reference->id]))->one()) {
                return null;
            }

            $newRecord = true;
            $element = new Element([
                'reference_id' => $this->reference->id,
                'section_id' => $section->id,
                'external_id' => $external_id,
                'status' => ModelHelper::STATUS_ACTIVE,
            ]);
            $element->loadDefaultValues(false);

            if (isset($item['picture'])) {
                $element->detail_picture = Reference::makeFileArray($item['picture']);
            }
        }

        $element->setAttributes($attributes);

        $properties = [];
        if (isset($item['brand'])) {
            $arProperty = $this->checkProperty('Бренд');
            $properties[$arProperty['id']] = $this->checkPropertyValue($arProperty, $item['brand']);
        }

        if (isset($item['properties'])) {
            foreach ($item['properties'] as $propName => $arItemProps) {
                $arProperty = $this->checkProperty($propName);
                $intCount = 0;
                $arValues = [];
                foreach ($arItemProps as $itemPropValue) {
                    $arValues['n' . $intCount] = $this->checkPropertyValue($arProperty, $itemPropValue);
                    $intCount++;
                }

                $properties[$arProperty['id']] = $arValues;
            }
        }

        $element->property_values = $properties;
        $transaction = $element::getDb()->beginTransaction();
        try {
            $res = $element->save();
            if ($res === false) {
                throw new \Exception('Ошибка сохранения элемента');
            } else {
                if ($this->reference->is_catalog === 'Y') {
                    $product = $this->getProduct($element->id);
                    $res = $product->save();
                    if ($res === false) {
                        throw new \Exception('Ошибка сохранения элемента');
                    }

                    $prices = $product->prices;
                    foreach ($this->priceTypes as $type) {
                        if (in_array($type->external_id, ['price', 'oldprice']) && $item[$type->external_id]) {
                            if (isset($prices[$type->id])) {
                                $prices[$type->id]->setAttribute('price', doubleval($item[$type->external_id]));
                            } else {
                                $prices[$type->id] = new Price(['price_type' => $type->id, 'price' => doubleval($item[$type->external_id]), 'currency' => 'RUB']);
                            }
                        }
                    }

                    if (isset($prices['oldprice']) && isset($prices['price'])) {
                        if ($prices['oldprice']->price < $prices['price']->price) {
                            unset($prices['oldprice']);
                        }
                    }

                    foreach ($prices as $price) {
                        if (strlen($price->price) > 0) {
                            if ($price->validate()) {
                                $price->link('product', $product);
                            } else {
                                throw new \Exception('Ошибка сохранения элемента');
                            }
                        }
                    }
                }
            }

            $transaction->commit();
            $k = $newRecord ? 'add' : 'upd';
            $counter[$k]++;

        } catch (\Exception $exception) {
            $transaction->rollBack();
            $counter['err']++;
        }

        return $element;
    }


    protected function getRemoteSections($url, $token, $depth = 2, $sections = [], $last_id = 0)
    {
        $array = json_decode($this->sendRequest($url, $token, ['depth' => $depth, 'sections' => implode(',', $sections), 'last_id' => $last_id]));

        return ArrayHelper::toArray($array);
    }

    protected function getRemoteProperties($url, $token, $last_id = 0)
    {
        $sections = ArrayHelper::getValue($this->params, 'import_sections');
        $array = json_decode($this->sendRequest($url, $token, ['last_id' => $last_id, 'sections' => implode(',', $sections)]));

        return ArrayHelper::toArray($array);
    }

    /**
     * @param       $url
     * @param       $token
     * @param array $params
     *
     * @return string
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected function sendRequest($url, $token, $params = [])
    {
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport'
        ]);

        $data = [
            'access-token' => $token,
        ];

        foreach ($params as $name => $param) {
            $data[$name] = $param;
        }

        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->setData($data)
            ->setHeaders([
                'Accept' => 'application/json',
            ])->send();

        if ($response->isOk) {
            return $response->content;
        } else {
            throw new Exception('Invalid remote server response');
        }
    }

    protected function getArrayCrc(array $array)
    {
        $c = crc32(print_r($array, true));
        if ($c > 0x7FFFFFFF)
            $c = -(0xFFFFFFFF - $c + 1);

        return $c;
    }

    /**
     * @param integer $id
     *
     * @return Product
     */
    protected function getProduct($id)
    {
        if ($product = Product::find()->where(['id' => $id])->with('prices')->one()) {
            return $product;
        }

        $product = new Product(['id' => $id]);
        $product->loadDefaultValues();

        return $product;
    }
}