<?php

namespace vilderr\baseproducts\widgets\assets;

use vilderr\main\web\AssetBundle;

/**
 * Class ImportAsset
 * @package vilderr\baseproducts\widgets\assets
 */
class ImportAsset extends AssetBundle
{
    public $sourcePath = '@vendor/vilderr/baseproducts/widgets/assets/src';

    public $js = [
        'js/import.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}