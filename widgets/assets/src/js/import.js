let running = false;

function DoNext(NS) {
    "use strict";

    let url = '';

    if (running) {
        let request = $.ajax({
            type: "POST",
            url: url,
            data: NS
        });

        request.done(function (result) {
            $('#import-result-container').html(result);
        });
    }

}

StartImport = function (ns) {
    running = document.getElementById('start-button').disabled = true;
    DoNext(ns);
};

EndImport = function () {
    running = document.getElementById('start-button').disabled = false;
};