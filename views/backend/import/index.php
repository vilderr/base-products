<?php

use yii\web\View;
use vilderr\baseproducts\Module;
use vilderr\baseproducts\widgets\Import;

/**
 * @var View $this
 * @var Module $module
 */

?>
<?= Import::widget(['params' => $module->importParams]); ?>
