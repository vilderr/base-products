<?php

namespace vilderr\baseproducts\models;

use Yii;

/**
 * This is the model class for table "{{%baseproducts_import_tree}}".
 *
 * @property int $id
 * @property int $element_id
 */
class ImportTree extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%baseproducts_import_tree}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['element_id'], 'required'],
            [['element_id'], 'integer'],
            [['element_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'element_id' => 'Element ID',
        ];
    }
}
