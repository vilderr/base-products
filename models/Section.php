<?php

namespace vilderr\baseproducts\models;

/**
 * Class Section
 * @package vilderr\baseproducts\models
 *
 * @property integer $base_section_id
 */
class Section extends \vilderr\reference\models\section\Section
{
    public function rules()
    {
        $rules = array_merge(parent::rules(), ['base_section_id' => ['base_section_id', 'integer']]);

        return $rules;
    }
}