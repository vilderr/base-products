<?php

return [
    'Baseproducts module' => 'Главный модуль',

    'all' => '<label>Всего</label>: {value}',
    'add' => '<label>Добавлено</label>: {value}',
    'upd' => '<label>Обновлено</label>: {value}',
    'err' => '<label>С ошибками</label>: {value}',
    'nav' => '<label>Отсутствующих</label>: {value}',
    'crc' => '<label>Обработано</label>: {value}',
    'del' => '<label>Удалено</label>: {value}',
    'dea' => '<label>Деактивировано</label>: {value}',
];