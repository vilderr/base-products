<?php

namespace vilderr\baseproducts;

use vilderr\main\Yii;
use yii\base\BootstrapInterface;

/**
 * Class Module
 * @package vilderr\baseproducts
 */
class Module extends \vilderr\main\base\Module implements BootstrapInterface
{
    public $importParams = [];

    public function bootstrap($app)
    {
        $app->i18n->translations["modules/baseproducts/*"] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath' => '@vendor/vilderr/baseproducts/messages',
            'fileMap' => [
                'modules/baseproducts/module' => 'module.php'
            ],
        ];

        if ($app->id == 'backend') {
            $app->urlManager->addRules([
                [
                    'class' => 'yii\\web\\GroupUrlRule',
                    'prefix' => 'baseproducts',
                    'routePrefix' => 'baseproducts',
                    'rules' => [
                        'import' => 'import/index'
                    ]
                ]
            ]);
        }
    }

    public static function t($message, $params = [], $category = 'module', $language = null)
    {
        return Yii::t('modules/baseproducts/' . $category, $message, $params, $language);
    }

    public function getName()
    {
        return self::t('Baseproducts module');
    }

    public function getMenuItems()
    {
        $items = [
            'baseproducts' => [
                'label' => 'База товаров',
                'icon' => 'database',
                'items' => [
                    [
                        'label' => 'Импорт товаров',
                        'url' => ['/baseproducts/import/index']
                    ]
                ]
            ]
        ];

        return $items;
    }
}