<?php

use yii\db\Migration;

/**
 * Class m190128_073044_update_reference_section_table
 */
class m190128_073044_update_reference_section_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%reference_section}}', 'base_section_id', $this->integer());
        $this->createIndex('{{%idx-reference_section-base_section_id}}', '{{%reference_section}}', ['reference_id', 'base_section_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('{{%idx-reference_section-base_section_id}}', '{{%reference_section}}');
        $this->dropColumn('{{%reference_section}}', 'base_section_id');
    }
}
