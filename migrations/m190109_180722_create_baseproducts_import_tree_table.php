<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%baseproducts_import_tree}}`.
 */
class m190109_180722_create_baseproducts_import_tree_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%baseproducts_import_tree}}', [
            'id' => $this->primaryKey(),
            'element_id' => $this->integer()->notNull()->unique(),
        ], $tableOptions);

        $this->createIndex('{{%idx-baseproducts_import_tree-element_id}}', '{{%baseproducts_import_tree}}', 'element_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%baseproducts_import_tree}}');
    }
}
