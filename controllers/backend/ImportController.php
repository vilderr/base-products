<?php

namespace vilderr\baseproducts\controllers\backend;

use yii\helpers\Url;
use vilderr\main\web\Controller;

/**
 * Class ImportController
 * @package vilderr\baseproducts\controllers\backend
 */
class ImportController extends Controller
{
    public function actionIndex()
    {
        $this->view->title = 'Импорт товаров';
        $this->view->params['breadcrumbs'][] = [
            'label' => 'База товаров',
            'url' => Url::toRoute('/baseproducts/')
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'module' => $this->module
        ]);
    }
}