Baseproducts module
===================
Baseproducts module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist vilderr/baseproducts "*"
```

or add

```
"vilderr/baseproducts": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \vilderr\baseproducts\AutoloadExample::widget(); ?>```